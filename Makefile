CC = gcc
CFLAGS = -std=c11 -pedantic-errors -Wall -Wextra -Werror -O0 -g
OBJS = common.o ising.o bits.o utilities.o
HEADERS = ising.h bits.h utilities.h

ising:  $(OBJS) $(HEADERS) depend
	$(CC) $(CFLAGS) -o ising $(OBJS) -lm

depend:
	$(CC) $(CFLAGS) -E -MM *.c > .depend

include .depend

tags:
	etags *.[ch] > TAGS

clean:
	rm -f *.o ising
