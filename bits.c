#include "bits.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

extern size_t dimension;

/* Bits */

bool bits_equal(bit one, bit two)
/*
 * Arguments:  Two  struct bits, not necessarily distinct
 * Returns:    TRUE if the bitvalues are identical; FALSE otherwise
 */
{
    return one.bitvalue == two.bitvalue;
}

bool negate(bit *b)
/*
 * Argument:  A pointer to a  struct bit
 * Returns:   Complemented bitvalue of argument
 */
{
    return !b->bitvalue;
}

void dump_bit(bit b)
/*
 * Argument:  A  struct bit
 * Effect:    Print the values of all the fields of the given  struct bit
 * */
{
    printf("x := %zd\n", b.x);
    printf("y := %zd\n", b.y);
    printf("energy := %d\n", b.energy);
    printf("bitvalue := %d\n", b.bitvalue);
}


void flip(bit *b)
/*
 * Argument:  A pointer to a  struct bit
 * Effect:    Complement the bitvalue of argument
 */
{
    b->bitvalue = negate(b);
}

bit complement(bit *b)
/*
 * Argument:  A pointer to a  struct bit
 * Returns:   A new  struct bit identical to the argument, except with
 *            complemented bitvalue
 */
{
    bit complement;

    /* Copy all fields... */
    complement.x = b->x;
    complement.y = b->y;
    complement.energy = b->energy;
    /* ...but complement the bitvalue */
    complement.bitvalue = negate(b);

    return complement;
}


/* Lattices */

bit **allocate_lattice(bit **lattice)
/*
 * Argument:  An uninitialized pointer to pointer to  struct bit

 * Returns:   Zeroed block of memory big enough to hold
 *                dimension * dimension
 *            struct bits
 */
{
    size_t i;

    /* Allocate the rows of the lattice */
    errno = 0;
    lattice = calloc(dimension, sizeof(bit *));
    if (lattice == NULL)
    {
	fprintf(stderr, "%s\n", strerror(errno));
	exit(EXIT_FAILURE);
    }

    /* Allocate the columns of the lattice */
    for (i = 0; i < dimension; i++)
    {
	errno = 0;
	lattice[i] = calloc(dimension, sizeof(bit));
	if (lattice[i] == NULL)
	{
	    fprintf(stderr, "%s\n", strerror(errno));
	    exit(EXIT_FAILURE);
	}
    }

    return lattice;
}

void free_lattice(bit **lattice)
/*
 * Argument:  A pointer to pointer to  struct bit; initialized  struct bit
 *            lattice
 * Effect:    Release the memory allocated to the lattice back to the heap
 */
{
    size_t i;

    for (i = 0; i < dimension; i++)
    {
	free(lattice[i]);
    }

    free(lattice);
}

void print_lattice (bit **lattice)
/*
 * Argument:  A pointer to pointer to  struct bit; initialized  struct bit
 *            lattice
 * Effect:    Print the bitvalues of the  struct bits of the given lattice
 *            as a two-dimensional matrix
 */
{
    size_t i, j;

    for (i = 0; i < dimension; i++)
    {
	/* Print rows */
	for (j = 0; j < dimension; j++)
	{
	    printf("%d", (lattice[i][j]).bitvalue);
	}
	putchar('\n');
    }
}

#if 0
bool lattices_equal(bit **a, bit **b)
/*
 * Arguments:  Two pointers to pointer to  struct bits; initialized  struct bit
 *             lattices
 * Returns:    TRUE if the lattices are equal; FALSE otherwise
 */
{
    size_t i, j;
    bool equal = true;

    for (i = 0; i < dimension; i++)
    {
	for (j = 0; j < dimension; j++)
	{
	    if (!bits_equal(a[i][j], b[i][j]))
	    {
		equal = false;
	    }
	}
    }

    return equal;
}
#endif
