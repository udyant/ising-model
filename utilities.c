#include "utilities.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

bool random_bit(void)
/*
 * Returns:  Either TRUE or FALSE, i.e., 1 or 0 */
{
    return rand() % 2;
}

bool is_all_zeroes(char *string)
/*
 *  Argument:  A string
 *  Returns:   TRUE if all characters in the string are '0', FALSE otherwise */
{
    char *sp;

    for (sp = string; *sp != '\0'; sp++)
    {
	if (*sp != '0')
	{
	    return false;
	}
    }

    return true;
}

bool is_positive_integer(char *string)
/*
 *  Argument:  A string
 *  Returns:   TRUE if all characters in the string are decimal digits,
 *             FALSE otherwise
 */
{
    char *sp;

    for (sp = string; *sp != '\0'; sp++)
    {
	if (!isdigit(*sp))
	{
	    return false;
	}
    }

    return !is_all_zeroes(string);
}

/* From /C: A Reference Manual/, 5th ed., by Harbison and Steele
 * page 352
 */
int how_many(const char *s, int c)
/*
 * Arguments:  A string and a character
 * Returns:    The number of occurences of the character within the string */
{
    int n = 0;
    if (c == 0)
    {
	return 0;
    }
    while (s)
    {
	s = strchr(s, c);
	if (s)
	{
	    n++;
	    s++;
	}
    }
    return n;
}

size_t find(const char string[], char character, size_t offset)
/*
 * Arguments:  A string not intended to be modified, a character, and an offset
 *             in the string
 * Returns:    The position of the first occurence of the character within the
 *             string after offset */
{
    size_t length = strlen(string);
    size_t i;

    for (i = offset; i < length; i++)
    {
	if (string[i] == character)
	{
	    return i;
	}
    }

    return (size_t) -1;
}

char *copy_substring(char *substring, const char *string, size_t start, size_t end)
/*
 * Arguments:  An uninitialized string that will house the desired substring,
 *             the given string, the position of the start of the substring, and
 *             the position of the end of the substring.
 * Returns:    The pointer to the desired substring
 */
{
    /* The length of the substring.  We add 1 for the NUL */
    size_t length = end - start + 1;

    errno = 0;
    substring = calloc(1 + length, sizeof *substring);
    if (substring == NULL)
    {
	fprintf(stderr, "copy_substring: %s\n", strerror(errno));
	exit(EXIT_FAILURE);
    }

    strncpy(substring, string + start, length - 1);
    substring[length] = '\0';

    return substring;
}
