#ifndef ISING_H
#define ISING_H

#include <stdbool.h>

/* Core */
int     check_and_add_neighbor(bit b, size_t x, size_t y);
int     energy(bit b);
void    calculate_energies(void);
void    ising_flip(bit *b);
bool    iterate(void);
void    print_iteration(long count);

/* Housekeeping */
size_t  count_zeroes(bit **ising_lattice);

#endif
