#ifndef BITS_H
#define BITS_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct bit {
    size_t x;		/* The x-coordinate or the column */
    size_t y;		/* The y-coordinate or the row */
    int energy;		/* The energy of this bit */
    bool bitvalue;		/* The value this bit: 0 or 1 */
} bit;

/* Bits */
bool bits_equal(bit one, bit two);
bool negate(bit *b);
void dump_bit(bit b);

void flip(bit *b);
bit  complement(bit *b);

/* Lattices */
bit  **allocate_lattice(bit **lattice);
void free_lattice(bit **lattice);
void print_lattice(bit **lattice);

/*
  bool lattices_equal(bit **a, bit **b);
*/

#endif
