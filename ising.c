#include "bits.h"
#include "utilities.h"

#include <math.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

/* The square Ising lattice that we work on */
static bit **ising_lattice;

extern double beta;
extern size_t dimension;
extern int scale;
extern size_t minority_size;

/* Format strings for various stages of the simulation
 *     initial_format    is used just after the lattice has been initialized;
 *     iteration_format  is used during the various configurations en route to
 *                       the final state
 *     final_format      is used after attainment of the ending configuration.
 */
static const char initial_format[] = "Initial configuration %10ld";
static const char iteration_format[] = "Iteration %10ld";
static const char final_format[] = "Final configuration %10ld";


/* Core */
int check_and_add_neighbor(bit b, size_t x, size_t y)
/*
 * Arguments:  A  struct bit, and the coordinates of a neighbor in the lattice.
 * Returns:    The amount of energy that this bit feels with this neighbor.
 */
{
    /* Check if the neighbor falls within the boundaries of the lattice */
    if (x < dimension && y < dimension)
    {
	/* See if the bitvalues disagree */
	if (b.bitvalue != (ising_lattice[x][y]).bitvalue)
	{
	    /* They do, add a unit of energy for the given bit */
	    return 1;
	}
	/* They don't */
    }

    /* Add no unit of energy for the given bit */
    return 0;
}

int energy(bit b)
/*
 * Argument:  A  struct bit
 * Returns:   The energy that the given bit feels with its immediate neighbors
 */
{
    int energy = 0;

    /* Check the neighbor bits lying due north, south, east, and west */
    energy += check_and_add_neighbor(b, b.x + 1, b.y);
    energy += check_and_add_neighbor(b, b.x - 1, b.y);
    energy += check_and_add_neighbor(b, b.x, b.y + 1);
    energy += check_and_add_neighbor(b, b.x, b.y - 1);

    return energy;
}

void calculate_energies(void)
/*
 * Effect:  Compute the energies of all bits in the Ising lattice */
{
    size_t i, j;

    for (i = 0; i < dimension; i++)
    {
	for (j = 0; j < dimension; j++)
	{
	    (ising_lattice[i][j]).energy = energy(ising_lattice[i][j]);
	}
    }
}

void ising_flip(bit *b)
/*
 * Argument:  A pointer to a  struct bit
 * Effect:    Flip the given bit and calculate its energy anew.  If the energy
 *            goes down, keep the flipped bit.  Otherwise, keep the flipped bit with
 *            an exponetially decaying probability
 */
{
    int old_energy = b->energy;
    int new_energy = energy(complement(b));
    int delta_energy = new_energy - old_energy;

    if  (delta_energy > 0)
    {
	double ising_threshold = exp(-(beta * delta_energy));
	double random_value = rand() % scale / (double) scale;

	if (ising_threshold < random_value)
	{
	    flip(b);
	}
    }
    else
    {
	flip(b);
    }
}

size_t count_zeroes(bit **lattice)
/*
 * Argument:  A pointer to pointer to struct bit
 * Returns:   The number of bits in the lattice with bitvalue 0
 */
{
    size_t count = 0;
    size_t i, j;

    for (i = 0; i < dimension; i++)
    {
	for (j = 0; j < dimension; j++)
	{
	    if ((lattice [i] [j]).bitvalue == 0)
	    {
		count++;
	    }
	}
    }

    return count;
}

bool iterate(void)
/*
 * Effect:  Choose a random bit in the lattice and attempt to flip it with the
 *          Ising criterion in  ising_flip(), which see.  Recompute the energies
 *          of the lattice bits.
 */
{
    bool keep_running = true;
    size_t x = (size_t) rand() % dimension;
    size_t y = (size_t) rand() % dimension;
    size_t zeroes = count_zeroes(ising_lattice);
    size_t ones = (dimension * dimension) - zeroes;

    ising_flip(&ising_lattice[x][y]);
    calculate_energies();

    if (zeroes == minority_size || ones == minority_size)
    {
	keep_running = false;
    }

    return keep_running;
}

void print_iteration(long count)
/*
 * Arguments:  A format string mentioned at the beginning of this source file:
 *                 initial_format,
 *                 iteration_format,
 *                 final_format,
 *             and the count of the number of iterations of the simulation
 *             thus far
 * Effect:     Print a header for the current iteration before printing the
 *             iteration proper
 */
{
    size_t i;
    size_t fmtstrlen;
    size_t zeroes = count_zeroes(ising_lattice);
    size_t ones = (dimension * dimension) - zeroes;
    const char *format_string;

    if (count == 0)
    {
	format_string = initial_format;
    }
    else if (zeroes == minority_size || ones == minority_size)
    {
	format_string = final_format;
    }
    else
    {
	format_string = iteration_format;
    }

    /* Print the header */
    fmtstrlen = strlen(format_string);
    putchar('\n');
    printf(format_string, count);
    putchar('\n');
    for (i = 0; i < fmtstrlen + 5; i++)
    {
	putchar('-');
    }
    putchar('\n');

    /* Print the actual lattice as a 2D matrix */
    print_lattice(ising_lattice);
}


/* Return codes:
 * 0    Successful execution
 * 1    Memory allocation failed
 * 2    Wrong form of argument
 */
int main(int argc, char *argv[])
{
    /* Error handling is the first and foremost thing to do
     * (Any arguments after the first three have no effect and are not
     * seen.)
     */
    if (argc > 3)
    {
	char *endptr;
	/* There are (at least) three arguments.  We now ensure that
	 * the size of the minority is a good and valid value, i.e.,
	 * a positive integer */
	if (is_positive_integer(argv[3]) || is_all_zeroes(argv[3]))
	{
	    minority_size = (size_t) strtoul(argv[3], &endptr, 10);
	}
	else
	{
	    fputs("minority_size <argv[3]> is not a nonnegative integer\n", stderr);
	    exit(2);
	}

	/* if (minority_size < 0) { */
	/* 	minority_size = 0; */
	/* } */
	if (minority_size > dimension * dimension)
	{
	    minority_size = dimension * dimension;
	}

	/* A minority greater in strength than half the number of
	 * lattice cells is not much of a minority. */
	else if (minority_size > (dimension * dimension) / 2)
	{
	    minority_size = (dimension * dimension) - minority_size;
	}
    }
    if (argc > 2)
    {
	/* There are (at least) two arguments.  We now ensure that
	 * the value of the thermodynamic beta or the inverse
	 * temperature is a positive floating point number of the form
	 *         <integral>.<fractional>
	 */

	/* Name the second argument for convenience. */
	char *beta_string = argv[2];
	size_t length = strlen(beta_string);

	/* There should be only one decimal point in the string */
	if (how_many(beta_string, '.') == 1)
	{
	    /* Determine its position */
	    size_t position_decimal = find(beta_string, '.', 0);

	    char *integral_part = NULL;
	    char *fractional_part = NULL;


	    /* Check the integral part */
	    integral_part = copy_substring(integral_part, \
					   beta_string,   \
					   0,				\
					   position_decimal - 1);
	    if (is_positive_integer(integral_part) ||	\
		is_all_zeroes(integral_part))
	    {
		/* Check the fractional part */
		fractional_part = copy_substring(fractional_part, \
						 beta_string,		\
						 position_decimal + 1,	\
						 length - 1);
		if (is_positive_integer(fractional_part) || \
		    is_all_zeroes(fractional_part))
		{
		    beta = atof(argv[2]);
		}
		else
		{
		    /* The fractional part of the
		     * floating-point number is negative,
		     * so error
		     */

		    fputs("minority_size <argv[3]> is not a nonnegative integer\n", stderr);
		    exit(2);
		}

		free(fractional_part);
	    }
	    else
	    {
		/* The integral part of the floating-point
		 * number is negative, so error
		 */

		fputs("beta <argv[2]> is not positive floating-point number.\n", stderr);
		exit(2);
	    }

	    free(integral_part);
	}
	else
	{
	    /* More than one decimal point found, so error */
	    fputs("beta <argv[2]> is not positive floating-point number.\n", stderr);
	    exit(2);
	}
    }
    if (argc > 1)
    {
	/* There is (at least) one argument.  We now ensure that the
	 * value of the dimension for the Ising lattice is a positive
	 * integer
	 */
	if (is_positive_integer(argv[1]))
	{
	    char *endptr;
	    dimension = (size_t) strtoul(argv[1], &endptr, 10);
	}
	else
	{
	    /* The value is nonpositive, so error */
	    fputs("dimension <argv[1]> is not a nonnegative integer.\n", stderr);
	    exit(2);
	}
    }

    /* Seed the random number generator of the standard library with
     * value of the current time
     */
    srand((unsigned int) time(0));

    size_t i, j;
    bit b;

    /* Allocate the square Ising lattice */
    ising_lattice = allocate_lattice(ising_lattice);
    for (i = 0; i < dimension; i++)
    {
	for (j = 0; j < dimension; j++)
	{
	    b.x = i;
	    b.y = j;
	    b.energy = 0;
	    b.bitvalue = random_bit();

	    ising_lattice[i][j] = b;
	}
    }
    /* Compute the energies of all cells in the lattice */
    calculate_energies();


    /* Begin simulation */
    print_iteration(0);

    /* Keep a count of the number of iterations thus far */
    long k = 1;
    while (iterate())
    {
	print_iteration(k++);
    }

    /* End simulation and release the memory allocated to the
     * Ising lattice back to the operationg system */
    free_lattice(ising_lattice);

    /* Return an error code of 0, indicating successful execution of the
     * simulation */
    return 0;
}
