#ifndef UTILITIES_H
#define UTILITIES_H

#include <stdlib.h>
#include <stdbool.h>

bool    random_bit(void);
bool    is_all_zeroes(char *string);
bool    is_positive_integer(char *string);
int     how_many(const char *s, int c);
size_t  find(const char string[], char character, size_t offset);
char    *copy_substring(char *substring, const char *string, size_t start, size_t end);

#endif
