#include <stdlib.h>

/* Inverse temperature or thermodynamic beta.  Lower values tend to
 * cause rapid changes, while higher values tend to retain the state of
 * the system
 */
double beta = 0.5;

/* We use square lattices.  This determines the side of the square.
 * Note that large lattices can take quite a while to exhibit phase
 * transitions to equilibrium configurations.
 */
size_t dimension = 3;

/* Affects the threshold for flipping the given bit.  Low values will
 * increase the chance of flipping. 
*/
int scale = 1000;

/* Number of lattice elements that should exist for the simulation to
 * terminate.  So, with the default size of 1, the simulation finishes
 * as soon as there is just one 1 or one 0 in the entire lattice.
 */
size_t minority_size = 1;
